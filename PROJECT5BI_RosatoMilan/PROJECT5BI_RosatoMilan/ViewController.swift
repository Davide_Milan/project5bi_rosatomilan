//
//  ViewController.swift
//  PROJECT5BI_RosatoMilan
//
//  Created by Studente on 22/11/2018.
//  Copyright © 2018 RosatoMilan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var testoPrimoOtteto: UITextField!
    @IBOutlet weak var testoSecondoOtteto: UITextField!
    @IBOutlet weak var testoTerzoOtteto: UITextField!
    @IBOutlet weak var testoQuartoOtteto: UITextField!
    @IBOutlet weak var labelRisultato: UILabel!
    var ottetiIP = [Int]()
    var risultato = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func calcolaIP(_ sender: Any) {
        ottetiIP[0] = fromStringBinToInt(testoPrimoOtteto.text)
        ottetiIP[1] = fromStringBinToInt(testoSecondoOtteto.text)
        ottetiIP[2] = fromStringBinToInt(testoTerzoOtteto.text)
        ottetiIP[3] = fromStringBinToInt(testoQuartoOtteto.text)
        
        for i in 0...3{
            risultato += String(ottetiIP[i]) + "."
        }
        //risultato.replacingCharacters(in: <#T##RangeExpression#>, with: <#T##StringProtocol#>)
        labelRisultato.text = risultato
    }
  
}

